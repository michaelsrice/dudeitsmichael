<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['name'];

    public function blogs()
    {
        return $this->belongsToMany(Blog::class);
    }
}
