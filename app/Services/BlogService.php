<?php

namespace App\Services;

use App\Blog;

class BlogService
{
    public function create($data)
    {
        // Create slug
        $slug = $this->createSlug($data['title']);

        // Create snippet
        $snippet = str_split($data['body'], 255)[0];

        $data = array_merge(
            $data,
            ['slug'     => $slug],
            ['snippet'  => $snippet]
        );
        $blog = Blog::create($data);

        if (isset($data['tags']) && !empty($data['tags'])) {
            $blog->tags()->attach($data['tags']);
        }

        return $blog;
    }

    public function update($id, $data)
    {
        $blog = Blog::findOrFail($id);

        $slug = $this->createSlug($data['title']);
        $snippet = str_split($data['body'], 255)[0];

        $data = array_merge(
            $data,
            ['slug'     => $slug],
            ['snippet'  => $snippet]
        );

        $blog->update($data);

        if (isset($data['tags']) && !empty($data['tags'])) {
            $blog->tags()->sync($data['tags']);
        }

        return $blog;
    }

    private function createSlug($title)
    {
        $slug = strtolower(str_replace(' ', '-', $title));
        return preg_replace('/[^A-Za-z0-9\-]/', '', $slug);
    }
}
