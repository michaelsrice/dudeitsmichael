<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $baseRules = [
            'body'      => 'required|max:4294967295',
            'published' => 'required|boolean',
            'tags'      => 'nullable|array',
            'tags.*'    => 'numeric'
        ];

        switch ($this->method()) {
            case 'POST':
                $methodRules = [
                    'title' => 'required|max:255|unique:blogs,title',
                ];
                break;
            case 'PUT':
                $methodRules = [
                    'title' => [
                        'required',
                        'max:255',
                        Rule::unique('blogs')->ignore($this->route('blog')),
                    ]
                ];
                break;
            default:
                $methodRules = [];
                break;
        }

        return array_merge($baseRules, $methodRules);
    }
}
