<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'snippet',
        'body',
        'published'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
