<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $me = new User;
        $me->first_name  = 'Michael';
        $me->last_name   = 'Rice';
        $me->email       = 'me@dudeitsmichael.com';
        $me->password    = bcrypt('password');
        $me->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
