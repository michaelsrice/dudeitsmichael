<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dude, it's Michael</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="/css/prism.css">
</head>
<body>
    <nav class="navbar navbar-default">
      <div class="container">
      
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">DIM</a>
        </div>

        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="{{ route('blog.index') }}">Blog</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ route('dashboard.index') }}">Login</a></li>
          </ul>
        </div>

      </div><!-- /.container -->
    </nav>

    <div class="jumbotron">
      <div class="container">
        <h1>Michael Rice</h1>
        <p>Welcome to my site</p>
      </div>
    </div>

    <div class="container">
      <div class="col-sm-8">
        @yield('content')
      </div>
      <div class="col-sm-4">
        @section('sidebar')
            <div class="panel panel-primary">
              <div class="panel-heading">Tags</div>
              <div class="panel-body">
                  <ul>
                      @foreach($tags as $tag)
                          <li>
                              <a href="{{ route('blog.index', 'tag='.$tag->name) }}">{{ $tag->name }}</a>
                          </li>
                      @endforeach
                  </ul>
              </div>
          </div>
        @show
      </div>
    </div>

    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="/js/prism.js"></script>
</body>
</html>
