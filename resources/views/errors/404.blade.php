@extends('layouts.basic')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h1>Duuuuude</h1>
            <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/rLsUo6seUss?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
            <p>I couldn't find the page you were looking for. Hopefully this makes it a little better</p>
            <a href="{{ route('home') }}">Go Home</a>
        </div>
    </div>
</div>
@endsection