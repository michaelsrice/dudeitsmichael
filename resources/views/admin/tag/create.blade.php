@extends('layouts.auth')

@section('main')
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb">
              <li><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
              <li><a href="{{ route('tags.index') }}">Tags</a></li>
              <li class="active">Create</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <form action="{{ route('tags.store') }}" method="POST">
                {{ csrf_field() }}

                <div class="panel panel-primary">
                    <div class="panel-heading">Create Tag</div>
                    <div class="panel-body">

                        <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                            @if($errors->has('name'))
                                <p class="help-text text-danger">{{ $errors->first('name') }}
                            @endif
                        </div>

                        <button type="submit" class="btn btn-success">Save</button>

                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection