@extends('layouts.auth')

@section('main')
<div class="container">

    @if (session('success'))
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">
                    <p>{{ session('success') }}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <ol class="breadcrumb">
              <li><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
              <li class="active">Blogs</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            @if(count($blogs) < 1)
                <div class="panel panel-primary">
                    <div class="panel-body text-center">
                        <h2>You ain't got no Blogs.</h2>
                        <a href="{{ route('blogs.create') }}" class="btn btn-primary btn-lg">Create one</a>  
                    </div>
                </div>
            @else
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <a href={{ route('blogs.create') }} class="btn btn-default btn-sm pull-right">Create Blog</a>
                        <h1>Blogs</h1>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Published</th>
                                    <th>Created On</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($blogs as $blog)
                                <tr>
                                    <td>{{ $blog->title }}</td>
                                    <td>{{ $blog->published ? 'Yup' : 'Nope' }}</td>
                                    <td>{{ $blog->created_at }}</td>
                                    <td>
                                        <form action="{{ route('blogs.destroy', $blog->id) }}" method="POST" class='delete-form'>
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger pull-right btn-delete">Delete</button>
                                        </form>
                                        <a href="{{ route('blogs.edit', $blog->id) }}" class="btn btn-warning pull-right">Edit</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content panel-danger">
      <div class="modal-header panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>Soft delete this?</p>
      </div>
      <div class="modal-footer panel-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger fersure-delete">Delete</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('scripts')
@parent

<script>
    $(document).ready(function () {
        var form = '';

        $('.delete-form').on('submit', function (e) {
            e.preventDefault();
            form = $(this);
            $('#myModal').modal();
        });

        $('.fersure-delete').on('click', function (){
            form[0].submit();
        });
    });
</script>
@endsection