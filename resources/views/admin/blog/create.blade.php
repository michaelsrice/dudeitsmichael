@extends('layouts.auth')

@section('css')
    @parent

    <link rel="stylesheet" href="/css/redactor.css">
@endsection

@section('main')
<div class="container">

    <div class="row">
        <div class="col-sm-12">

            <ol class="breadcrumb">
              <li><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
              <li><a href="{{ route('blogs.index') }}">Blogs</a></li>
              <li class="active">Create</li>
            </ol>

           @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
    
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h1>Create Blog</h1>
                </div>
                <div class="panel-body">
                    <form action="{{ route('blogs.store') }}" method="POST">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea name="body"></textarea>
                        </div>

                        <div class="form-group">
                            <label>Publish
                            <div class="radio">
                              <label>
                                <input type="radio" name="published" value="1">
                                Yes
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="published" value="0" checked>
                                No
                              </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <select name="tags[]" class="form-control" multiple >
                                @foreach($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection


@section('scripts')
    @parent
    <script src="/js/redactor.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $('textarea[name="body"]').redactor({
            minHeight:  300
        });
    });
    </script>
@endsection
