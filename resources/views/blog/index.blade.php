@extends('layouts.master')

@section('content')
    @foreach($blogs as $blog)
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>
                <a href="{{ route('blog.showBySlug', $blog->slug) }}">
                    {{ $blog->title }}
                </a>
            </h2>
            <p>{{ $blog->created_at->format('F dS, Y') }}</p>
        </div>
        <div class="panel-body">
            <p>{!! $blog->snippet !!}</p>
            <a href="{{ route('blog.showBySlug', $blog->slug) }}">View Post</a>
        </div>
    </div>
    @endforeach
@endsection
