@extends('layouts.master')

@section('content')
<h1>{{ $blog->title }}</h1>
<p>{{ $blog->created_at->format('F dS, Y') }}</p>

{!! $blog->body !!}
@endsection